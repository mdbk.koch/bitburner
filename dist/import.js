var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const config = {
    repo: "https://gitlab.com/mdbk.koch/bitburner/-/raw/main/",
    remoteFilesDir: "dist/",
    gameFilesDir: "/scripts/",
    knownFileNames: ["utils.js"],
    importFileName: "import.js",
};
export function main(ns) {
    return __awaiter(this, void 0, void 0, function* () {
        yield updateImporter(ns);
        yield syncScripts(ns);
    });
}
const updateImporter = (ns) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("test sync");
    ns.print("Updating importer...");
    const isDownloadSuccess = yield ns.wget(`${config.repo}${config.remoteFilesDir}${config.importFileName}`, `${config.gameFilesDir}${config.importFileName}`);
    if (!isDownloadSuccess) {
        ns.print(`Error: Download import file failed - args: ${{ url: `${config.repo}${config.remoteFilesDir}${config.importFileName}`, }}`);
        ns.exit();
    }
    else
        ns.print(`Success: Download import file success - file location: ${config.gameFilesDir}${config.importFileName}`);
});
const syncScripts = (ns) => __awaiter(void 0, void 0, void 0, function* () {
    let isSuccess = true;
    for (const knowFile in config.knownFileNames) {
        isSuccess = (yield ns.wget(`${config.repo}${config.remoteFilesDir}${knowFile}`, `${config.gameFilesDir}${knowFile}`)) && isSuccess;
    }
    if (!isSuccess) {
        ns.print(`Error: Sync files failed`);
        ns.exit();
    }
    else
        ns.print(`Success: Synced files success`);
});

import { NS } from "../index";

const config = {
    repo: "https://gitlab.com/mdbk.koch/bitburner/-/raw/main/",
    remoteFilesDir: "dist/",
    gameFilesDir: "/scripts/",
    knownFileNames: ["utils.js"],
    importFileName: "import.js",
}

export async function main(ns: NS) {
    await updateImporter(ns);
    await syncScripts(ns);
}

const updateImporter = async (ns: NS) => {
    console.log("test sync");
    ns.print("Updating importer...");
    const isDownloadSuccess = await ns.wget(`${config.repo}${config.remoteFilesDir}${config.importFileName}`, `${config.gameFilesDir}${config.importFileName}`);
    if (!isDownloadSuccess) {
        ns.print(`Error: Download import file failed - args: ${{ url: `${config.repo}${config.remoteFilesDir}${config.importFileName}`, }}`);
        ns.exit();
    } else
        ns.print(`Success: Download import file success - file location: ${config.gameFilesDir}${config.importFileName}`);
}

const syncScripts = async (ns: NS) => {
    let isSuccess = true;
    for (const knowFile in config.knownFileNames) {
        isSuccess = await ns.wget(`${config.repo}${config.remoteFilesDir}${knowFile}`, `${config.gameFilesDir}${knowFile}`) && isSuccess;
    }
    if (!isSuccess) {
        ns.print(`Error: Sync files failed`);
        ns.exit();
    } else
        ns.print(`Success: Synced files success`);

}